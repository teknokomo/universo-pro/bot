'use strict';

// Необходимо в Nginx отключить кеширование для ответов бота

const express = require('express');
const axios = require('axios');
const cors = require('cors');

// На будущее. Для валидации и зачистки входящей информации.
// https://express-validator.github.io/docs/index.html

const BOT_INTERFACE = process.env.BOT_INTERFACE || '127.0.0.1';
const BOT_PORT = process.env.BOT_PORT || 3007;

// 283943071 - Туманов
// 424504212 - Перов
// -1001762382623 - чат Разработка кода
// 5772 - топик "Обучение" чата "Разработка и автоматизация".
// 8657 - топик "Служба технической поддержки" чата "Разработка и автоматизация".
const BOT_DEVELOPERS_CHAT_ID = -1001762382623;
const BOT_MESSAGE_THREAD_ID = process.env.BOT_MESSAGE_THREAD_ID || 5772;
const BOT_STP_THREAD_ID = 8657;

const app = express();

app.use(express.json());                          // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true }));  // to support URL-encoded bodies

app.use(cors({
  origin: '*'
}));

// Добавление промежуточного ПО.
// // Add headers before the routes are defined
// app.use(function (req, res, next) {

//   // Website you wish to allow to connect
//   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888');

//   // Request methods you wish to allow
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

//   // Request headers you wish to allow
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

//   // Set to true if you need the website to include cookies in the requests sent
//   // to the API (e.g. in case you use sessions)
//   res.setHeader('Access-Control-Allow-Credentials', true);

//   // Pass to next layer of middleware
//   next();
// });

// export BOT_TELEGRAM_TOKEN=СКОПИРОВАТЬ_ИЗ_ТЕЛЕГРАМ
const BOT_TELEGRAM_TOKEN = process.env.BOT_TELEGRAM_TOKEN;
console.log(BOT_TELEGRAM_TOKEN);
if (! BOT_TELEGRAM_TOKEN) {
  console.log('Не указан ключ Телеграм. Выход.');
  return 1;
}
const GITLAB_TOKEN = process.env.GITLAB_TOKEN || 'pY1SbVSgvz7f7t1V';

// const GITLAB_WEBHOOK_KEY = process.env.GITLAB_WEBHOOK_KEY;

///// Блок функций для отправки /////

let send = (method, data) => {
  axios.post(`https://api.telegram.org/bot${BOT_TELEGRAM_TOKEN}/${method}`, data)
  .then(function (response) {
    console.log('send then', response.data);
  })
  .catch(function (error) {
    console.log('send catch', JSON.stringify(error, null, 2));
  });
}

let sendMessage = data => {
  send(
    'sendMessage',
    data
  );
}

let sendSticker = data => {
  send(
    'sendSticker',
    data
  );
}

/* Запрос проектов с Сириусо
query{
  projektojProjekto(objId:21
  ){
    edges {
      node {
        uuid
        nomo{enhavo}
        priskribo{enhavo}
        tasko{
          edges{node{
            nomo{enhavo}
            priskribo{enhavo}
          }}
        }
      }
    }
  }
}
*/

let getProjects = (body) => {
  let text = `Текущие проекты:
*Мир Великого Кольца*
Создаваемая с нуля киновселенная (виртуальные миры), со множеством проектов, которые будут формировать у людей правильный образ человека будущего. Мы противостоим медиа-продуктам из западных стран, которые сейчас практически сплошь несут идеи деградации человечества.
- Информация в [ВК](https://vk.com/topic-214008597_48949601).
- Контактное лицо Владимир Левадный @Vladimir\\\_Levadnij генеральный разнорабочий.

*РВМ Универсо*
Для того чтобы в эпоху цифровизации спасти человечество от порабощения и уничтожения, для того чтобы все технологии поставить на благо трудящемуся человечеству, дать каждому человеку инструменты, знания и мотивацию, чтобы создавать Народные предриятия, Профсоюзы, Советы, творческие проекты и многое другое, мы создаём Реальный виртуальный мир Универсо (RVM Universo).
- Телеграм канал [РВМ Универсо](https://t.me/universo_pro).
- Информация в [ВК](https://vk.com/topic-203394770_49359948).
- Посмотреть [исходный код](https://gitlab.com/teknokomo/hyperuniverse).
- Контактное лицо Павел Перов @MrAvathar разработчик фронтэнда на JS (Vue, Quasar).

*Сириусо*
Бекенд для РВМ Универсо. Хранение и предоставление данных.
- Посмотреть [исходный код](https://gitlab.com/teknokomo/siriuso).
- Контактное лицо Василий Манжула @VasMih разработчик бекенда на Python (Django).

*Социальная сеть*
Сеть предназначена для объединения людей, которые ищут единомышленников для реализации своих проектов и приближения Коммунизма.
- Посмотреть [исходный код](https://gitlab.com/teknokomo/siriuso).
Контактные лица:
- Александр Туманов @justice1980 разработка пользовательского интерфейса.
- Василий Манжула @VasMih разработчик бекенда на Python (Django).
- Павел Перов @MrAvathar разработчик фронтэнда на JS (Vue, Quasar).

*Списки покупок*
Совместное семейное ведение списков покупок для того чтобы не забывать что надо купить.
Каждый список могут заполнять и редактировать несколько человек.
- Посмотреть [исходный код](https://gitlab.com/teknokomo/universo-pro/cart).
Контактные лица:
- Александр Туманов @justice1980 - разработка фронта и бека.
- Павел Перов @MrAvathar разработчик фронта на JS (Vue, Quasar).
- Василий Манжула @VasMih - разработка бека
- Александр Сидоров тестирование.
- Александр Хилевич тестирование.

*Роботун*
Информационный бот Телеграм, для привлечения новых людей к разработке основного проекта Универсо и других проектов Технокома.
- Посмотреть [исходный код](https://gitlab.com/teknokomo/universo-pro/bot).
- Контактное лицо Александр Туманов @justice1980 разработчик бота на JS (Express).`;

  let data = {
    chat_id: body.message ? body.message.chat.id : body.callback_query.message.chat.id,
    text: text,
    parse_mode: 'Markdown',
  }

  if (body.message.message_thread_id) {
    data.message_thread_id = body.message.message_thread_id;
  }
  sendMessage(data);
}

////////////////////////////////////////////

app.get('/', (req, res) => {
  console.log('GET /');
  console.log(req.params);
  res.send('Hello World');
});

app.post('/', (req, res) => {
  console.log('POST /');
  console.log(req.params);
  // console.log(req.body);
  res.send('Hello World');
});

// Обработчик GET нужен только для проверки доступности пути /webhook.
app.get('/webhook/telegram', (req, res) => {
  console.log('GET /webhook/telegram');
  console.log(req.query);

  res.type('text/plain');
  res.send('Путь /webhook/telegram доступен');
});

// Сервер Телеграм присылает всё через POST.
// В req.params попадает то, что записывается через ":", например, /webhook/:test_path, добавит test_path.
// В req.query попадает то, что идёт в части запроса в строке URL после "?"
// В req.body попадает то, что передаётся в теле POST-запроса
app.post('/webhook/telegram', (req, res) => {
  console.log('POST /webhook/telegram');
  console.log(JSON.stringify(req.body, null, 2));
  if (req.body.message) {
    let chat_id = req.body.message.chat.id;
    console.log('Chat ID:', chat_id);
    // if ('message_thread_id' in req.body.message) {
    //   // 5772 - топик "Обучение" чата "Разработка и автоматизация".
    //   let message_thread_id = req.body.message.message_thread_id;
    // }

    // Если в строке текста передавали какие-нибудь команды, то они попадают в массив entities
    if (
      req.body.message.entities
      && req.body.message.entities.length !== 0
    ) {
      // В массив req.body.message.entities попадают все строки, которые сервер интерпретирует,
      // как один из специальных типов:
      // mention - упоминание пользователя (@username)
      // hashtag - хештег (#hashtag)
      // cashtag - кештег или код валюты ($USD)
      // bot_command - команда для бота (/menu@vodonos_roboto_pro_bot или просто /menu)
      // url - URL (https://vodonos.roboto.pro/)
      // email - адрес электронной почты (vodonos@roboto.pro)
      // phone_number - номер телефона (+7(987)123-4567)
      // bold - жирный шрифт
      // italic - наклонный шрифт
      // underline - подчёркнутый текст
      // strikethrough - зачёркнутый текст
      // spoiler - spoiler message (замазанное сообщение)
      // code - моноширинная строка текста (форматирование через меню клиента)
      // pre - моноширинный блок текста (```)
      // text_link - для ссылок, у которых не виден URL (форматирование через меню клиента)
      // text_mention - имя пользователя, которого не существует.
      let entities = req.body.message.entities;
      console.log(req.body.message.entities);

      for (let i = 0; i < req.body.message.entities.length; i++) {
        console.log(i + ' ' + entities[i].type);
        if (entities[i].type === 'bot_command') {
          console.log('Запись: ' + i);
          console.log(entities[i]);
          console.log('offset: ' + entities[i].offset);
          console.log('length: ' + entities[i]['length']);
          console.log('Строка: ' + req.body.message.text);
          let bot_command = req.body.message.text.substring(entities[i].offset, entities[i].offset + entities[i].length);
          console.log('bot_command: ' + bot_command);
// Список команд для @BotFather
// get_projects - Получить все проекты
          // Если серверу Телеграм ничего не возвращать, а выходить из функции с помощью return, то
          // сервер Телеграм будет считать, что его сообщение НЕ доставлено и будет пытаться отправить
          // его повторно каждые две минуты.
          switch(bot_command) {
            // Стандартная команда запуска бота
            case '/start':
              // Ничего не делать
            break;
      
            // Получить список проектов
            case '/get_projects':
            case '/get_projects@universo_pro_bot':
              getProjects(req.body);
            break;
      
            default:
              console.log('Команда ' + bot_command + ' не распознана');
              console.log(req.body.message.text);
              sendMessage({
                chat_id: req.body.message.chat.id,
                text: 'Команда ' + bot_command + ' не распознана'
              });

              // Отладка
              if (req.body.message.chat.id != 283943071) {
                sendMessage({
                  chat_id: "283943071",
                  text: 'Команда ' + bot_command + ' не распознана'
                });
              }
          }
        }
      }
    }
  }
  else if (req.body.callback_query) {
    let callback_query_id = req.body.callback_query.id;
    console.log('callback_query_id: ' + callback_query_id);
    let callback_query_data = req.body.callback_query.data;
    let command = callback_query_data.split('_')[0];
    switch (command) {
      case 'getProjects':
        getProjects(req.body);
      break;

      default:
        sendMessage({
          chat_id: "283943071",
          text: "Кнопка не распознана"
        });
    }

    axios.post(`https://api.telegram.org/bot${BOT_TELEGRAM_TOKEN}/answerCallbackQuery`, {
      callback_query_id: callback_query_id,
      text: req.body.callback_query.data
    })
    .then(function (response) {
      console.log('response');
    })
    .catch(function (error) {
      console.log('error');
    });
  }
  res.send('wh');
});

// GitLab

let createIssueEventMarkdownText = (data) => {
  let assignee = data.assignees[0] ? data.assignees[0].name : 'не назначен';
  let text = `Информация о задаче
Проект: [${data.project.name}](${data.project.web_url})
Задача: [${data.object_attributes.iid}](${data.object_attributes.url})
Заголовок: ${data.object_attributes.title}
Описание: ${data.object_attributes.description}
Состояние: ${data.object_attributes.state}
Постановщик: ${data.user.name}
Исполнитель: ${assignee}`;

  return text;
}

let createPushEventMarkdownText = (data) => {
  let commits = [];
  let index = 1;
  // if () {
    for (let commit of data.commits) {
      console.log('commit', commit);
      commits.push(`${index}. [${commit.title}](${commit.url})`);
      index++;
    }
  // }
  let commits_text = commits.join("\n");

  let text = `📣 Изменения в проекте
—————————————
Автор: ${data.user_username} (${data.user_name})
Проект: [${data.project.name}](${data.project.web_url})
*Список изменений*:
${commits_text}`;

  return text;
}

let createBuildEventMarkdownText = (data) => {
  let status_string = data.build_status;
  switch(status_string) {
    case 'success':
      status_string += ' 🟢';
      break;
    case 'failed':
      status_string += ' 🔴';
      break;
  }
  
  let duration = data.build_duration || 0;

//   let text = `Информация о задаче
// Проект: [${data.project.name}](${data.project.web_url}) +
// Задача: ${data.build_name}
// Этап: ${data.build_stage} +
// Статус: ${status_string} +
// Продолжительность: ${duration} сек +
// Разработчик: ${data.user.name}`; +

  let text = `Информация о задаче сборки
Проект: [${data.project.name}](${data.project.web_url})
Задача: -
Этап: ${data.build_stage}
Статус: ${status_string}
Продолжительность: ${duration} сек
Разработчик: ${data.user.name}`;

  return text;
}

let createPipelineEventMarkdownText = (data) => {
  let status_string = data.object_attributes.status
  switch(data.object_attributes.status) {
    case 'scheduled':
      status_string += ' ⏰';
      break;
    case 'pending':
      status_string += ' ⏳';
      break;
    case 'running':
      status_string += ' ⏱';
      break;
    case 'success':
      status_string += ' 🟢';
      break;
    case 'failed':
      status_string += ' 🔴';
      break;
  }

  let duration = data.object_attributes.duration || 0;

  // событие pipeline
  let text = `Информация о процессе сборки
Проект: [${data.project.name}](${data.project.web_url})
Сборка: [${data.object_attributes.id}](${data.object_attributes.url})
Статус: ${status_string}
Продолжительность: ${duration} сек
Разработчик: ${data.user.name}`;

// https://docs.gitlab.com/ee/api/pipelines.html
// created, waiting_for_resource, preparing, pending, running, success, failed, canceled, skipped, manual, scheduled

  return text;
}

app.get('/webhook/gitlab', (req, res) => {
  console.log('GET /webhook/gitlab');
  console.log(req.query);

  res.type('text/plain');
  res.send('Путь /webhook/gitlab доступен');
});

app.post('/webhook/gitlab', (req, res) => {
  console.log('POST /webhook/gitlab');
  console.log('req.query', req.query);

  console.log('req.headers', JSON.stringify(req.headers, null, 2));
  console.log('req.body', JSON.stringify(req.body, null, 2));

  if (req.headers['x-gitlab-token'] != GITLAB_TOKEN) {
    console.log('Получен секретный ключ', req.headers['x-gitlab-token']);
    res.type('text/plain');
    res.status(401);
    res.send('401 - укажите правильный секретный ключ');

    return 1;
  }

  let text = '';
  switch(req.body.object_kind) {
    case 'push':
      text = createPushEventMarkdownText(req.body);
      break;

    case 'issue':
      text = createIssueEventMarkdownText(req.body);
      break;

    case 'build':
      text = createBuildEventMarkdownText(req.body);
      break;
    
    case 'pipeline':
      text = createPipelineEventMarkdownText(req.body);
      break;
    
    default:
      console.log('x-gitlab-event', req.headers['x-gitlab-event']);
      res.type('text/plain');
      res.status(400);
      res.send(`400 - событие ${req.body.object_kind} сейчас не обрабатывается`);
  
      return 1;
  }

  console.log('text', text);

  // 283943071 - Туманов
  // -1001762382623 - чат Разработка кода
  // 5772 - топик "Обучение" чата "Разработка и автоматизация".
  let message = {
    text: text,
    parse_mode: 'Markdown',
  };

  let project_id = req.body.project.id || 0;
  if (project_id == 40822052) {
    message.chat_id = 283943071;
  }
  else {
    message.chat_id = -1001762382623;
    message.message_thread_id = BOT_MESSAGE_THREAD_ID;
  }

  // sendMessage({
  //   chat_id: -1001762382623,
  //   message_thread_id: BOT_MESSAGE_THREAD_ID,
  //   text: text,
  //   parse_mode: 'Markdown',
  // });

  console.log('message', JSON.stringify(message, null, 2));

  sendMessage(message);

  res.send('wh');
});

app.post('/api/issue', (req, res) => {
  console.log('POST /api/issue');
  console.log('req.query', req.query);

  console.log('req.headers');
  console.log(JSON.stringify(req.headers, null, 2));
  console.log(JSON.stringify(req.body, null, 2));

  res.type('application/json');
  
  // // Вебсайты, которым разрешено подключаться
  // res.setHeader('Access-Control-Allow-Origin', '*');

  // // Разрешённые методы
  // res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // // Разрешённые заголовки
  // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // // Можно присылать куки
  // res.setHeader('Access-Control-Allow-Credentials', true);

  if (req.headers['authorization'] == undefined) {
    // res.setHeader('Content-Type', 'application/json');
    res.status(401).send(JSON.stringify({
      "success": false,
      "error_description": "Не указан токен безопасности"
    }));
    return 1;
  }

  if (req.headers['authorization'] != 'Bearer nI70SPzlKupoEXI3') {
    // res.setHeader('Content-Type', 'application/json');
    res.status(401).send(JSON.stringify({
      "success": false,
      "error_description": "Токен безопасности не прошёл проверку"
    }));
    return 1;
  }

  // Получить и обработать заголовок Authorization
  // Authorization: Bearer nI70SPzlKupoEXI3
  
  // Ожидаемые данные
  // {
  //   "description": ""
  // }

  let text = "*Запрос техподдержки с сайта Универсо*\n" + req.body.description + "\n";

  if (req.body.context) {
    if (req.body.context.path != "") {
      text += "Прислано со страницы: *" + req.body.context.path + "*\n";
    }

    if (req.body.context.contact != "") {
      text += "Контактное лицо: *" + req.body.context.contact + "*\n";
    }
  }

  text.trim();

  // {
  //   "description": "тест",
  //   "context": {
  //     "contact": "",
  //     "path": "/"
  //   }
  // }

  sendMessage({
    chat_id: BOT_DEVELOPERS_CHAT_ID,
    message_thread_id: BOT_STP_THREAD_ID,
    text: text,
    parse_mode: 'Markdown',
  });

  res.type('application/json');
  res.send(JSON.stringify({
    "success": true,
    "error_description": ""
  }));
});

// пользовательская страница 404
app.use(function(req, res, next) {
  res.type('text/plain');
  res.status(404);
  res.send('404 — Не найдено');
});

app.listen(BOT_PORT, BOT_INTERFACE);
console.log(`running on http://${BOT_INTERFACE}:${BOT_PORT}`);
