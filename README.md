# Информационный бот
[[_TOC_]]

## Описание
Бот Телеграм, для привлечения новых людей к разработке основного проекта Универсо и других проектов Технокома.
Для разработки используется фреймворк Express.

## Запуск бота
Для запуска в виде сервиса используется менеджер процессов [PM2](https://pm2.keymetrics.io/).
```
export BOT_TELEGRAM_TOKEN=YOURBOTTELEGRAMTOKEN
pm2 start main.js
```
